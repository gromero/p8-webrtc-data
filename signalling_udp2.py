import asyncio
import json

class SignalingServerProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.client_address = None
        self.server_address = None

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received {message} from {addr}")

        if message == "REGISTER CLIENT":
            self.client_address = addr
            print(f"Registered client at {addr}")
        elif message == "REGISTER SERVER":
            self.server_address = addr
            print(f"Registered server at {addr}")
        else:
            message_json = json.loads(message)
            if "sdp" in message_json:
                if self.server_address and message_json["type"] == "offer":
                    self.transport.sendto(data, self.server_address)
                    print(f"Forwarded offer to {self.server_address}")
                elif self.client_address and message_json["type"] == "answer":
                    self.transport.sendto(data, self.client_address)
                    print(f"Forwarded answer to {self.client_address}")
            elif message_json.get("type") == "bye":
                if self.server_address:
                    self.transport.sendto(data, self.server_address)
                if self.client_address:
                    self.transport.sendto(data, self.client_address)

    def connection_made(self, transport):
        self.transport = transport
        print("Signaling server started")

    def connection_lost(self, exc):
        print("Connection lost")

async def main():
    print("Starting signaling server")
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignalingServerProtocol(),
        local_addr=('0.0.0.0', 9999))
    try:
        await asyncio.sleep(3600)  # Run for 1 hour
    finally:
        transport.close()

asyncio.run(main())
