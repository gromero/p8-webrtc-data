import asyncio
import json
import time
from aiortc import RTCPeerConnection, RTCSessionDescription

time_start = None


def current_stamp():
    global time_start
    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc, signaling):
    await signaling.connect()
    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        for _ in range(3):  # Send three messages
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)
        await signaling.send({"type": "bye"})  # Send BYE message
        print("Sent BYE message")

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")
        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    await pc.setLocalDescription(await pc.createOffer())
    await signaling.send({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})

    while True:
        message = await signaling.receive()
        if "sdp" in message:
            desc = RTCSessionDescription(sdp=message["sdp"], type=message["type"])
            await pc.setRemoteDescription(desc)
        elif message.get("type") == "bye":
            print("Received BYE message, exiting")
            break


class UDPSignaling(asyncio.DatagramProtocol):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.transport = None
        self.response_future = None

    async def connect(self):
        loop = asyncio.get_running_loop()
        self.transport, _ = await loop.create_datagram_endpoint(
            lambda: self,
            remote_addr=(self.host, self.port))
        await self.send("REGISTER CLIENT")

    async def send(self, message):
        if isinstance(message, dict):
            message = json.dumps(message)
        self.transport.sendto(message.encode())

    def datagram_received(self, data, addr):
        message = json.loads(data.decode())
        if self.response_future and not self.response_future.done():
            self.response_future.set_result(message)

    async def receive(self):
        self.response_future = asyncio.get_running_loop().create_future()
        return await self.response_future


if __name__ == "__main__":
    signaling = UDPSignaling('127.0.0.1', 9999)
    pc = RTCPeerConnection()
    coro = run_offer(pc, signaling)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        signaling.transport.close()  # Remove await from close()
