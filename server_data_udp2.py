import asyncio
import json
from aiortc import RTCPeerConnection, RTCSessionDescription

async def run_answer(pc, signaling):
    await signaling.connect()

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")
        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")
            if isinstance(message, str) and message.startswith("ping"):
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    while True:
        message = await signaling.receive()
        if "sdp" in message:
            desc = RTCSessionDescription(sdp=message["sdp"], type=message["type"])
            await pc.setRemoteDescription(desc)
            if message["type"] == "offer":
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})
        elif message.get("type") == "bye":
            print("Received BYE message, exiting")
            break

class UDPSignaling(asyncio.DatagramProtocol):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.transport = None
        self.response_future = None

    async def connect(self):
        loop = asyncio.get_running_loop()
        self.transport, _ = await loop.create_datagram_endpoint(
            lambda: self,
            remote_addr=(self.host, self.port))
        await self.send("REGISTER SERVER")

    async def send(self, message):
        if isinstance(message, dict):
            message = json.dumps(message)
        self.transport.sendto(message.encode())

    def datagram_received(self, data, addr):
        message = json.loads(data.decode())
        if self.response_future and not self.response_future.done():
            self.response_future.set_result(message)

    async def receive(self):
        self.response_future = asyncio.get_running_loop().create_future()
        return await self.response_future

if __name__ == "__main__":
    signaling = UDPSignaling('127.0.0.1', 9999)
    pc = RTCPeerConnection()
    coro = run_answer(pc, signaling)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        signaling.transport.close()  # Remove await from close()
